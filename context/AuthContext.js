import createDataContext from "./createDataContext";
import * as SecureStore from "expo-secure-store";
import { Alert } from "react-native";

const authReducer = (state, action) => {
  switch (action.type) {
    case "RESTORE_TOKEN":
      return {
        userToken: action.token,
        isLoading: false,
      };
    case "SIGN_IN":
      return {
        isSignout: false,
        userToken: action.token,
      };
    case "SIGN_OUT":
      return {
        isSignout: true,
        userToken: null,
      };
    default:
      return state;
  }
};

const signUp = (dispatch) => {
  return async ({ name, username, email, password }) => {
    // console.log('Name: ' + name + ' Username: ' + username + ' Email: ' + email + ' Password: ' + password);
    try {
      let data = new FormData();
      data.append("email", email);
      data.append("name", name);
      data.append("password", password);
      data.append("username", username);
      data.append("country", "Bolivia");
      data.append("city", "Cochabamba");
      data.append("type", "user");

      const res = await fetch("https://slogan.com.bo/blssd/users/addMobile", {
        method: "POST",
        body: data,
      });

      console.log("Response: " + res);
      const json = await res.json();

      if (json.response.status) {
        Alert.alert("Success", "User created successfully");

        await SecureStore.setItemAsync("email", email);
        await SecureStore.setItemAsync("name", name);
        await SecureStore.setItemAsync("password", password);
        await SecureStore.setItemAsync("username", username);
        await SecureStore.setItemAsync("userId", String(json.response.data.id));
        await SecureStore.setItemAsync("type", "user");

        await SecureStore.setItemAsync("userToken", "dummy-auth-token");
        dispatch({ type: "SIGN_IN", token: "dummy-auth-token" });
      } else {
        Alert.alert("error");
      }
    } catch (error) {
      console.log(error);
    }
  };
};

const signIn = (dispatch) => {
  return async ({ username, password }) => {
    try {
      let data = new FormData();
      data.append("username", username);
      data.append("password", password);

      const res = await fetch("https://slogan.com.bo/blssd/users/loginMobile", {
        method: "POST",
        body: data,
      });

      const json = await res.json();

      if (json.response.status) {
        Alert.alert("Success", "User logged in successfully");

        await SecureStore.setItemAsync("username", username);
        await SecureStore.setItemAsync("password", password);
        await SecureStore.setItemAsync("userToken", "dummy-auth-token");

        dispatch({ type: "SIGN_IN", token: "dummy-auth-token" });
      } else {
        Alert.alert("error");
      }
    } catch (error) {
      console.log(error);
    }
  };
};

const signOut = (dispatch) => {
  return async () => {
    await SecureStore.deleteItemAsync("userToken");
    dispatch({ type: "SIGN_OUT" });
  };
};

const restoreToken = (dispatch) => {
  return ({ userToken }) => {
    console.log("UserToken: " + userToken);
    dispatch({ type: "RESTORE_TOKEN", token: userToken });
  };
};

export const { Provider, Context } = createDataContext(
  authReducer,
  { signIn, signOut, signUp, restoreToken },
  {
    isLoading: true,
    isSignout: false,
    userToken: null,
  }
);
