import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";

import { vw, vh} from 'react-native-expo-viewport-units';

export default function Splash() {
  return (
    
    <View style={componentStyles.container}>
      <View style={componentStyles.header}>
        <Text style={componentStyles.headerText}>Splash</Text>
      </View>
      <View style={componentStyles.body}>
        <Image
          source={require("../assets/img/SignLog/logo.png")}
          style={componentStyles.splashImage}
        />
      </View>
    </View>
  );
}
// COMPONENT END
// COMPONENT STYLES
const componentStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f5f5f5",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  header: {
    width: vw(100),
    height: vh(10),
    backgroundColor: "#f5f5f5",
    justifyContent: "center",
    alignItems: "center",
  },
  headerText: {
    fontSize: vw(5),
    fontWeight: "bold",
    color: "#000",
  },
  body: {
    width: vw(100),
    height: vh(100),
    backgroundColor: "#fff",
    borderWidth: 2,
    borderColor: "#bfbfbf",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  splashImage: {
    width: vw(100),
    height: vh(100),
    alignItems: "center",
    justifyContent: "center",
  },
});
// COMPONENT STYLES END
