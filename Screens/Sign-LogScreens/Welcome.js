import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
} from "react-native";

import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";

import { useNavigation } from "@react-navigation/native";

import LogoApple from "../../assets/svg/SignLog/apple.svg";
import LogoFacebook from "../../assets/svg/SignLog/facebook.svg";
import LogoGoogle from "../../assets/svg/SignLog/google.svg";

import { LinearGradient } from "expo-linear-gradient";

// COMPONENTS
const WelcomeText = () => {
  return (
    <View style={componentStyles.welcomeContainer}>
      <Text style={componentStyles.welcomeText}>WELCOME</Text>
    </View>
  );
};

const LogWays = () => {
  const navigation = useNavigation();
  return (
    <View style={componentStyles.Login}>
      <View style={componentStyles.AlternativeLogin}>
        <LinearGradient
          colors={["#AA4FFB", "#44ADFA"]}
          style={componentStyles.gradient}
        >
          <Text style={componentStyles.AlternativeLoginText}>Let's you in</Text>
          <View style={componentStyles.SocialNetworksLogin}>
            <TouchableOpacity
              style={componentStyles.SocialNetworksLoginButton}
              onPress={() => {
                alert("You are logged in with Google");
              }}
            >
              <View style={componentStyles.SocialNetworksLoginButtonContent}>
                <LogoGoogle
                  style={componentStyles.SocialNetworksLoginButtonImage}
                />
                <Text style={componentStyles.SocialNetworksLoginButtonText}>
                  Google
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={componentStyles.SocialNetworksLoginButton}
              onPress={() => {
                alert("You are logged in with Facebook");
              }}
            >
              <View style={componentStyles.SocialNetworksLoginButtonContent}>
                <LogoFacebook
                  style={componentStyles.SocialNetworksLoginButtonImage}
                />
                <Text style={componentStyles.SocialNetworksLoginButtonText}>
                  Facebook
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={componentStyles.SocialNetworksLoginButton}
              onPress={() => {
                alert("You are logged in with Apple ID");
              }}
            >
              <View style={componentStyles.SocialNetworksLoginButtonContent}>
                <LogoApple
                  style={componentStyles.SocialNetworksLoginButtonImage}
                />

                <Text style={componentStyles.SocialNetworksLoginButtonText}>
                  Apple
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </LinearGradient>
      </View>
      <View style={componentStyles.PasswordLogin}>
        <TouchableOpacity
          style={componentStyles.PasswordLoginButton}
          onPress={() => {
            navigation.navigate("Login");
          }}
        >
          <Text style={componentStyles.PasswordLoginButtonText}>
            Sign in with password
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const RegisterRedirection = () => {
  const navigation = useNavigation();

  return (
    <View style={componentStyles.RegisterRedirection}>
      <Text>Don't have an account? </Text>
      <Text
        style={{ color: "#0435F0" }}
        onPress={() => navigation.navigate("PersonalData")}
      >
        Sign Up
      </Text>
    </View>
  );
};
// COMPONENTS END

// COMPONENTS STYLES

const componentStyles = StyleSheet.create({
  welcomeContainer: {
    width: "100%",
    height: "30%",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "center",
  },
  welcomeText: {
    fontSize: 50,
    fontWeight: "bold",
    color: "#000",
  },

  Login: {
    width: "100%",
    height: "50%",
    flexDirection: "column",
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "center",
  },
  gradient: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  AlternativeLogin: {
    width: "70%",
    height: "70%",
    flexDirection: "column",
    backgroundColor: "#984aff",
    alignItems: "center",
    justifyContent: "space-around",
    borderRadius: 10,
    overflow: "hidden",
  },
  AlternativeLoginText: {
    color: "white",
    fontSize: 30,
    fontWeight: "bold",
  },
  SocialNetworksLogin: {
    width: "80%",
    height: "70%",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-around",
  },
  SocialNetworksLoginButton: {
    width: "100%",
    height: "20%",
    flexDirection: "row",
    backgroundColor: "#d9d9d9",
    alignItems: "center",
    justifyContent: "space-around",
    borderRadius: 5,
  },
  SocialNetworksLoginButtonContent: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    width: "100%",
  },
  SocialNetworksLoginButtonImage: {
    width: 25,
    height: 25,
  },
  SocialNetworksLoginButtonText: {
    color: "white",
    fontSize: 20,
  },
  PasswordLogin: {
    width: "100%",
    height: "20%",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  PasswordLoginButton: {
    width: "70%",
    height: "60%",
    backgroundColor: "#0a36ee",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  PasswordLoginButtonText: {
    color: "white",
    fontSize: 20,
  },
  RegisterRedirection: {
    width: "100%",
    height: "10%",
    flexDirection: "row",
    backgroundColor: "#fff",
    alignItems: "flex-start",
    justifyContent: "center",
  },
});

// COMPONENTS STYLES END

// MAIN COMPONENT
const Welcome = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={mainStyles.container}>
        <ImageBackground
          source={require("../../assets/img/SignLog/bg.png")}
          resizeMode="cover"
          style={mainStyles.image}
        >
          <WelcomeText />
          <LogWays />
          <RegisterRedirection />
        </ImageBackground>
      </View>
    </SafeAreaView>
  );
};

// MAIN COMPONENT END

// MAIN STYLES

const mainStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f5f5f5",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },

  image: {
    width: "100%",
    height: "100%",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
});

// MAIN STYLES END

export default Welcome;
