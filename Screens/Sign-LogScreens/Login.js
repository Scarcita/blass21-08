import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from "react-native";
import { useNavigation } from "@react-navigation/native";

import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";

import { Context } from "../../context/AuthContext";

import { Provider as AuthProvider } from "../../context/AuthContext";
import { Context as AuthContext } from "../../context/AuthContext";

import LogoBlssd from "../../assets/svg/SignLog/logo-blssd.svg";
import LogoTextoBlssd from "../../assets/svg/SignLog/textBlssd.svg";
import LogoApple from "../../assets/svg/SignLog/apple.svg";
import LogoFacebook from "../../assets/svg/SignLog/facebook.svg";
import LogoGoogle from "../../assets/svg/SignLog/google.svg";

// COMPONENTS
const Title = () => {
  return (
    <View style={componentStyles.titleContainer}>
      <LogoBlssd style={componentStyles.titleImage} />
      <LogoTextoBlssd style={componentStyles.textoBlssd} />
    </View>
  );
};

const Form = () => {
  const navigation = useNavigation();

  const { signIn } = React.useContext(AuthContext);

  const [username, onChangeUsername] = useState("");
  const [password, onChangePassword] = useState("");

  return (
    <View style={componentStyles.formContainer}>
      <View style={componentStyles.labelContainer}>
        <Text style={componentStyles.label}>Username</Text>
      </View>

      <View style={componentStyles.formInputContainer}>
        <TextInput
          style={componentStyles.formInput}
          onChangeText={onChangeUsername}
          value={username}
        />
      </View>

      <View style={componentStyles.labelContainer}>
        <Text style={componentStyles.label}>Password</Text>
      </View>

      <View style={componentStyles.formInputContainer}>
        <TextInput
          style={componentStyles.formInput}
          onChangeText={onChangePassword}
          value={password}
        />
      </View>

      <View style={componentStyles.checkboxContainer}>
        <View
          style={componentStyles.checkbox}
          onPress={() => {
            componentStyles.checkbox.backgroundColor = "red";
          }}
        ></View>
        <Text style={componentStyles.checkboxText}>Remember me</Text>
      </View>

      <View style={componentStyles.submitContainer}>
        <TouchableOpacity
          style={componentStyles.submitButton}
          onPress={() => {
            console.log("username: " + username + " password: " + password);
            signIn({ username: username, password: password });
          }}
        >
          <Text style={componentStyles.submitButtonText}>Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const AlternativeOptions = () => {
  const navigation = useNavigation();

  return (
    <View style={componentStyles.altLogContainer}>
      <View style={componentStyles.altLogTextContainer}>
        <Text style={componentStyles.altLogText}>or continue with</Text>
      </View>

      <View style={componentStyles.altLogOptions}>
        <View style={componentStyles.altLogOption}>
          <TouchableOpacity
            style={componentStyles.altLogOptionButton}
            onPress={() => {
              alert("You are logged in with Google");
            }}
          >
            <LogoGoogle style={componentStyles.altLogOptionButtonImage} />
          </TouchableOpacity>
        </View>

        <View style={componentStyles.altLogOption}>
          <TouchableOpacity
            style={componentStyles.altLogOptionButton}
            onPress={() => {
              alert("You are logged in with Facebook");
            }}
          >
            <LogoFacebook style={componentStyles.altLogOptionButtonImage} />
          </TouchableOpacity>
        </View>

        <View style={componentStyles.altLogOption}>
          <TouchableOpacity
            style={componentStyles.altLogOptionButton}
            onPress={() => {
              alert("You are logged in with Apple");
            }}
          >
            <LogoApple style={componentStyles.altLogOptionButtonImage} />
          </TouchableOpacity>
        </View>
      </View>

      <View style={componentStyles.existingAccount}>
        <View style={componentStyles.existingAccountTextContainer}>
          <Text style={componentStyles.existingAccountText}>
            Already have an account?
          </Text>
        </View>
        <View style={componentStyles.existingAccountButtonContainer}>
          <TouchableOpacity
            style={componentStyles.existingAccountButton}
            onPress={() => {
              navigation.navigate("PersonalData");
            }}
          >
            <Text style={componentStyles.existingAccountButtonText}>
              Sign In
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

// COMPONENTS END

// STYLES COMPONENT
const componentStyles = StyleSheet.create({
  titleContainer: {
    width: "100%",
    height: "30%",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  titleImage: {
    width: "60%",
    height: "50%",
  },

  textoBlssd: {
    width: "60%",
    height: "50%",
  },

  formContainer: {
    width: "80%",
    height: "40%",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  formInputContainer: {
    width: "100%",
    height: "15%",
    alignItems: "center",
    justifyContent: "center",
  },
  formInput: {
    width: "100%",
    height: "100%",
    borderWidth: 1,
    borderColor: "#0935f0",
    borderRadius: 10,
    paddingLeft: "5%",
    backgroundColor: "lightgray",
    fontSize: 16,
    textAlign: "center",
  },
  labelContainer: {
    width: "100%",
    height: "10%",
    alignItems: "flex-start",
    justifyContent: "flex-end",
  },
  label: {
    fontSize: 18,
    color: "#000",
  },
  checkboxContainer: {
    width: "100%",
    height: "10%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  checkbox: {
    width: "10%",
    height: "100%",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#0935f0",
    backgroundColor: "lightgray",
    alignItems: "center",
    justifyContent: "center",
    marginRight: "5%",
  },

  checkboxText: {
    fontSize: 16,
    color: "#000",
  },
  submitContainer: {
    width: "100%",
    height: "15%",
    alignItems: "center",
    justifyContent: "center",
  },
  submitButton: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#0935f0",
    borderRadius: 10,
  },
  submitButtonText: {
    fontSize: 22,
    fontWeight: "bold",
    color: "#fff",
  },

  altLogContainer: {
    width: "100%",
    height: "20%",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  altLogTextContainer: {
    width: "100%",
    height: "10%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  altLogText: {
    fontSize: 18,
    fontWeight: "bold",
    letterSpacing: 1,
    color: "#000",
  },
  altLogOptions: {
    width: "80%",
    height: "30%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  altLogOption: {
    width: "20%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  altLogOptionButton: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  altLogOptionButtonImage: {
    width: "100%",
    height: "100%",
    resizeMode: "contain",
  },

  existingAccount: {
    width: "100%",
    height: "20%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  existingAccountTextContainer: {
    width: "50%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  existingAccountText: {
    fontSize: 18,
    color: "#000",
  },
  existingAccountButtonContainer: {
    width: "15%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  existingAccountButton: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  existingAccountButtonText: {
    fontSize: 18,
    fontWeight: "bold",
    color: "blue",
  },
});
// STYLES COMPONENT END

// MAIN COMPONENT
const Login = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={mainStyles.container}>
        <Title />
        <Form />
        <AlternativeOptions />
      </View>
    </SafeAreaView>
  );
};
// MAIN COMPONENT END

// MAIN STYLES
const mainStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
  },
});
// MAIN STYLES END

export default Login;
