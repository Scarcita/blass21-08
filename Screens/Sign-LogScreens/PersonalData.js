import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from "react-native";
import { useNavigation } from "@react-navigation/native";

import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";

import { Context as AuthContext } from "../../context/AuthContext";

import LogoBlssd from "../../assets/svg/SignLog/logo-blssd.svg";
import LogoTextoBlssd from "../../assets/svg/SignLog/textBlssd.svg";
import LogoPerson from "../../assets/svg/SignLog/person.svg";
import LogoLeftArrow from "../../assets/svg/SignLog/leftArrow.svg";
import LogoTitle from "../../assets/svg/SignLog/title.svg";

import { LinearGradient } from "expo-linear-gradient";

// COMPONENTS
const Title = () => {
  return (
    <View style={componentStyles.titleContainer}>
      <LinearGradient
        // Button Linear Gradient
        colors={["#AA4FFB", "#44ADFA"]}
        style={componentStyles.gradient}
      >
        <View style={componentStyles.titleDos}>
          <View style={componentStyles.flecha}>
            <LogoLeftArrow style={componentStyles.left} />
          </View>
          <View style={componentStyles.titlePrincipal}>
            <LogoTitle style={componentStyles.title} />
          </View>
        </View>
        <LogoPerson style={componentStyles.titleImage} />
      </LinearGradient>
    </View>
  );
};

const Form = () => {
  const navigation = useNavigation();
  const { signUp } = React.useContext(AuthContext);

  const [name, onChangeName] = useState("");
  const [username, onChangeUsername] = useState("");
  const [email, onChangeEmail] = useState("");
  const [password, onChangePassword] = useState("");

  return (
    <View style={componentStyles.container}>
      <View style={componentStyles.formContainer}>
        <View style={componentStyles.labelContainer}>
          <Text style={componentStyles.label}>Name</Text>
        </View>

        <View style={componentStyles.formInputContainer}>
          <TextInput
            style={componentStyles.formInput}
            onChangeText={onChangeName}
            value={name}
          />
        </View>

        <View style={componentStyles.labelContainer}>
          <Text style={componentStyles.label}>Username</Text>
        </View>

        <View style={componentStyles.formInputContainer}>
          <TextInput
            style={componentStyles.formInput}
            onChangeText={onChangeUsername}
            value={username}
          />
        </View>

        <View style={componentStyles.labelContainer}>
          <Text style={componentStyles.label}>Email</Text>
        </View>

        <View style={componentStyles.formInputContainer}>
          <TextInput
            style={componentStyles.formInput}
            onChangeText={onChangeEmail}
            value={email}
          />
        </View>

        <View style={componentStyles.labelContainer}>
          <Text style={componentStyles.label}>Password</Text>
        </View>

        <View style={componentStyles.formInputContainer}>
          <TextInput
            style={componentStyles.formInput}
            onChangeText={onChangePassword}
            value={password}
          />
        </View>

        <View style={componentStyles.submitContainer}>
          <TouchableOpacity
            style={componentStyles.submitButton}
            onPress={() => {
              signUp({ name, username, email, password });
            }}
          >
            <Text style={componentStyles.submitButtonText}>Continue</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

// COMPONENTS END

// STYLES COMPONENT
const componentStyles = StyleSheet.create({
  titleContainer: {
    width: "100%",
    height: "30%",
    alignItems: "center",
    justifyContent: "space-evenly",
    backgroundColor: "blue",
  },
  gradient: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  titleDos: {
    flexDirection: "row",
    width: "100%",
    height: "30%",
  },
  flecha: {
    width: "100%",
    height: "100%",
  },
  left: {
    width: "100%",
    height: "100%",
  },
  titlePrincipal: {
    width: "100%",
    height: "30%",
  },
  title: {
    width: "100%",
    height: "30%",
  },

  titleImage: {
    width: "60%",
    height: "50%",
  },

  textoBlssd: {
    width: "60%",
    height: "50%",
  },
  container: {
    width: "100%",
    height: "70%",
    alignItems: "center",
    justifyContent: "center",
  },

  formContainer: {
    width: "80%",
    height: "80%",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  formInputContainer: {
    width: "100%",
    height: "10%",
    alignItems: "center",
    justifyContent: "center",
  },
  formInput: {
    width: "100%",
    height: "100%",
    borderWidth: 1,
    borderColor: "#0935f0",
    borderRadius: 10,
    paddingLeft: "5%",
    backgroundColor: "lightgray",
    fontSize: 16,
    textAlign: "center",
  },
  labelContainer: {
    width: "100%",
    height: "5%",
    alignItems: "flex-start",
    justifyContent: "flex-end",
  },
  label: {
    fontSize: 18,
    color: "#0935F0",
  },
  submitContainer: {
    width: "100%",
    height: "12%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "10%",
  },
  submitButton: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#0935F0",
    borderRadius: 10,
  },
  submitButtonText: {
    fontSize: 22,
    fontWeight: "bold",
    color: "#fff",
  },
});
// STYLES COMPONENT END

// MAIN COMPONENT
const PersonalData = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={mainStyles.container}>
        <Title />
        <Form />
      </View>
    </SafeAreaView>
  );
};
// MAIN COMPONENT END

// MAIN STYLES
const mainStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
});
// MAIN STYLES END

export default PersonalData;
