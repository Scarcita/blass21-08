import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  Button,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import * as SecureStore from "expo-secure-store";

import { vw, vh } from "react-native-expo-viewport-units";

import LogoBlssd from "../../assets/svg/SignLog/logo-blssd.svg";
import LogoTextoBlssd from "../../assets/svg/SignLog/textBlssd.svg";
import LogoGoogle from '../../assets/svg/SignLog/google.svg';
import LogoFacebook from '../../assets/svg/SignLog/facebook.svg';
import LogoApple from '../../assets/svg/SignLog/apple.svg';

// FUNCTIONS

async function save(mail, password, passwordConfirmation) {
  await SecureStore.setItemAsync(mail, password, passwordConfirmation);
}

async function getPasswordFor(mail, password, passwordConfirmation) {
  let resultMail = await SecureStore.getItemAsync(mail);
  let resultPassword = await SecureStore.getItemAsync(password);
  let resultPasswordConfirmation = await SecureStore.getItemAsync(
    passwordConfirmation
  );

  if (resultMail && resultPassword) {
    alert(
      "You are logged in with mail: " +
      resultMail +
      " and password: " +
      resultPassword +
      " and confimation password: " +
      resultPasswordConfirmation
    );
  } else {
    alert("No passwords stored under that mail.");
  }
}

// FUNCTIONS END

// COMPONENTS
const Title = () => {
  return (
    <View style={componentStyles.titleContainer}>
      <LogoBlssd
        style={componentStyles.titleImage}
      />
      <LogoTextoBlssd
        style={componentStyles.textoBlssd}
      />

    </View>
  );
};

const Form = () => {
  const data = { mail: mail, password: password };
  const navigation = useNavigation();

  const sendData = async () => {
    fetch("https://reqres.in/api/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Success:", data);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  // const getFromApi = async () => {
  //   try {
  //     const response = await fetch(
  //       'https://reqres.in/api/users' + id
  //     );
  //     const json = await response.json();
  //     return json.movies;
  //   } catch (error) {
  //     console.error(error);
  //   }
  // };

  const [mail, onChangeMail] = React.useState("Your mail here");
  const [password, onChangePassword] = React.useState("Your password here");
  const [passwordConfirmation, onChangePasswordConfirmation] = React.useState(
    "Repeat your password here"
  );

  return (
    <View style={componentStyles.formContainer}>
      <View style={componentStyles.form}>
        <Text style={componentStyles.formText}>Email</Text>
        <TextInput
          style={componentStyles.formInput}
          clearTextOnFocus
          onChangeText={(text) => onChangeMail(text)}
          mail={mail}
        />
      </View>
      <View style={componentStyles.form}>
        <Text style={componentStyles.formText}>Password</Text>
        <TextInput
          style={componentStyles.formInput}
          clearTextOnFocus
          onChangeText={(text) => onChangePassword(text)}
          password={password}
        />
      </View>
      <View style={componentStyles.form}>
        <Text style={componentStyles.formText}>Rewrite Password</Text>
        <TextInput
          style={componentStyles.formInput}
          clearTextOnFocus
          onChangeText={(text) => onChangePasswordConfirmation(text)}
          passwordConfirmation={passwordConfirmation}
        />
      </View>
      <View style={componentStyles.form}>
        <TouchableOpacity
          style={componentStyles.formButton}
          onPress={() => {
            console.log(mail + " " + password + " " + passwordConfirmation);
            sendData();
            save(mail, password, passwordConfirmation);
            navigation.navigate("Login");
          }}
        >
          <Text style={componentStyles.formButtonText}>Sign Up</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

// const AlternativeOptions = () => {
//   // const navigation = useNavigation();

//   const [mail, onChangeMail] = React.useState("Your mail here");
//   const [password, onChangePassword] = React.useState("Your password here");

//   return (
//     // <View style={componentStyles.alternativeOptionsContainer}>
//     //   <Text style={componentStyles.alternativeOptionsText}>
//     //     or continue with
//     //   </Text>
//     //   <View style={componentStyles.alternativeOptions}>
//     //     <TouchableOpacity
//     //       style={componentStyles.alternativeOptionsButton}
//     //       onPress={() => {
//     //         alert("You are logged in with Google");
//     //       }}
//     //     >
//     //       <View style={componentStyles.alternativeOptionsButtonContent}>

//     // < LogoGoogle       
//                style={componentStyles.alternativeOptionsButtonImage}
//                                      /> 
//     //         <Text style={componentStyles.alternativeOptionsButtonText}>
//     //           Google
//     //         </Text>
//     //       </View>
//     //     </TouchableOpacity>
//     //     <TouchableOpacity
//     //       style={componentStyles.alternativeOptionsButton}
//     //       onPress={() => {
//     //         alert("You are logged in with Facebook");
//     //       }}
//     //     >
//     //       <View style={componentStyles.alternativeOptionsButtonContent}> 
//             <LogoFacebook

//     //           style={componentStyles.alternativeOptionsButtonImage}

//                   />
//     //         
//     //         <Text style={componentStyles.alternativeOptionsButtonText}>
//     //           Facebook
//     //         </Text>
//     //       </View>
//     //     </TouchableOpacity>
//     //     <TouchableOpacity
//     //       style={componentStyles.alternativeOptionsButton}
//     //       onPress={() => {
//     //         alert("You are logged in with Apple ID");
//     //       }}
//     //     >
//     //       <View style={componentStyles.alternativeOptionsButtonContent}>
//     //         < LogoApple 
//     //           style={componentStyles.alternativeOptionsButtonImage}
//     //          />
//     //                                                                           
//     //         
//     //         <Text style={componentStyles.alternativeOptionsButtonText}>
//     //           Apple
//     //         </Text>
//     //       </View>
//     //     </TouchableOpacity>
//     //   </View>
//     //   <View style={componentStyles.existingAccount}>
//     //     <Text style={componentStyles.existingAccountText}>
//     //       Already have an account?
//     //     </Text>
//     //     <TouchableOpacity
//     //       style={componentStyles.existingAccountButton}
//     //       onPress={() => {
//     //         navigation.navigate("Register");
//     //       }}
//     //     >
//     //       <Text style={componentStyles.existingAccountButtonText}>Sign In</Text>
//     //     </TouchableOpacity>
//     //   </View>
//     // </View>

//     <View style={styles.container}>
//       {/* <Text style={styles.paragraph}>Save an item, and grab it later!</Text>
//       {}

//       <TextInput
//         style={styles.textInput}
//         clearTextOnFocus
//         onChangeText={(text) => onChangeMail(text)}
//         password={mail}
//       />
//       <TextInput
//         style={styles.textInput}
//         clearTextOnFocus
//         onChangeText={(text) => onChangePassword(text)}
//         password={password}
//       />
//       {}
//       <Button
//         title="Save this mail/password pair"
//         onPress={() => {
//           save(mail, password);
//           onChangeMail("Your mail here");
//           onChangePassword("Your password here");
//         }}
//       /> */}

//       <Text style={styles.paragraph}>🔐 Enter your mail 🔐</Text>
//       <TextInput
//         style={styles.textInput}
//         placeholder="Enter the mail for the password you want to get"
//       />
//       <TouchableOpacity
//         style={styles.button}
//         onPress={() => {
//           getPasswordFor(mail);
//         }
//         }
//       >
//         <Text style={styles.buttonText}>Get password</Text>
//       </TouchableOpacity>

//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: "center",
//     paddingTop: 10,
//     backgroundColor: "#ecf0f1",
//     padding: 8,
//   },
//   paragraph: {
//     fontSize: 18,
//     fontWeight: "bold",
//     textAlign: "center",
//   },
//   textInput: {
//     height: 35,
//     borderColor: "gray",
//     borderWidth: 0.5,
//     padding: 4,
//   },
// });

// COMPONENTS END

// STYLES COMPONENT
const componentStyles = StyleSheet.create({
  titleContainer: {
    width: "100%",
    height: "30%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "10%",
  },
  titleImage: {
    width: "60%",
    height: "100%",
  },
  textoBlssd: {
    width: "60%",
    height: "100%",

  },

  formContainer: {
    width: "100%",
    height: "40%",
    alignItems: "center",
    justifyContent: "center",
  },
  form: {
    width: "80%",
    height: "20%",
    marginTop: "5%",
    alignItems: "center",
    justifyContent: "center",
  },
  formText: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#000",
  },
  formInput: {
    width: "100%",
    height: "100%",
    borderColor: "#000",
    borderWidth: 1,
    borderRadius: 10,
    padding: "2%",
  },
  formCheckBox: {
    width: "40px",
    height: "40px",
    borderColor: "#000",
    borderWidth: 1,
    borderRadius: 10,
    padding: "2%",
  },
  formCheckBoxText: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#000",
  },
  formButton: {
    width: "100%",
    height: "50%",
    backgroundColor: "#000",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    marginTop: "5%",
  },
  formButtonText: {
    fontSize: 20,
    color: "#fff",
  },

  alternativeOptionsContainer: {
    width: "100%",
    height: "20%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "5%",
  },
  alternativeOptionsText: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#000",
  },
  alternativeOptions: {
    width: "80%",
    height: "10%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    marginTop: "5%",
  },
  alternativeOptionsButton: {
    width: "30%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    marginLeft: "2%",
  },
  alternativeOptionsButtonContent: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  alternativeOptionsButtonImage: {
    width: "50%",
    height: "50%",
  },
  alternativeOptionsButtonText: {
    fontSize: 20,
    color: "#000",
  },
  existingAccount: {
    width: "100%",
    height: "10%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "5%",
  },
  existingAccountText: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#000",
  },
  existingAccountButton: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  existingAccountButtonText: {
    fontSize: 20,
    color: "#000",
  },
});
// STYLES COMPONENT END

// MAIN COMPONENT
const Register = () => {
  return (
    <View style={mainStyles.container}>
      <View style={mainStyles.box}>
        <Title />
        <Form />
        {/* <AlternativeOptions /> */}
      </View>
    </View>
  );
};
// MAIN COMPONENT END

// MAIN STYLES
const mainStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  box: {
    width: vw(90),
    height: vh(90),
    backgroundColor: "transparent",
    borderWidth: 2,
    borderColor: "#bfbfbf",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
});
// MAIN STYLES END

export default Register;
