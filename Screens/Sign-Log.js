import react from "react";
import createStackNavigator from "@react-navigation/stack";

import Welcome from "./Sign-LogScreens/Welcome";
import Login from "./Sign-LogScreens/Login";
import Register from "./Sign-LogScreens/Register";
import PersonalData from "./Sign-LogScreens/PersonalData";
import Home from "./Home";

const Stack = createStackNavigator();

const SignLog = () => {
  return (
    <Stack.Navigator
        initialRouteName="Welcome"
        screenOptions={{
            headerShown: false
        }}
    >
      <Stack.Screen name="Welcome" component={Welcome} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="PersonalData" component={PersonalData} />
      <Stack.Screen name="Home" component={Home} />
    </Stack.Navigator>
  );
};

export default SignLog;