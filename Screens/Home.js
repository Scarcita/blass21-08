import react from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Feed from "./HomeComponents/Feed";
import Profile from "./HomeComponents/Profile";
import ProfileVerified from "./HomeComponents/ProfileVerified";
import Stories from "./HomeComponents/FeedComponents/Stories";
import StorieVideo from "./HomeComponents/FeedComponents/StorieVideo";
import CameraScreen from "./HomeComponents/FeedComponents/Camera";
import NewPost from "./HomeComponents/FeedComponents/NewPost";
import Notifications from "./HomeComponents/FeedComponents/Notifications";
import Comments from "./HomeComponents/FeedComponents/Comments";

const Stack = createStackNavigator();

const Home = () => {
  return (
    <Stack.Navigator
      initialRouteName="Feed"
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Feed" component={Feed} />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="ProfileVerified" component={ProfileVerified} />
      <Stack.Screen name="Stories" component={Stories} />
      <Stack.Screen name="StorieVideo" component={StorieVideo} />
      <Stack.Screen name="Camera" component={CameraScreen} />
      <Stack.Screen name="NewPost" component={NewPost} />
      <Stack.Screen name="Notifications" component={Notifications} />
      <Stack.Screen name="Comments" component={Comments} />
    </Stack.Navigator>
  );
};

export default Home;
