import React from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Image,
  TextInput,
} from "react-native";
import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";
import * as SecureStore from "expo-secure-store";

import { useNavigation } from "@react-navigation/native";
import { vw, vh } from "react-native-expo-viewport-units";
import LogoLove from "../../../assets/svg/Home/corazon.svg";
import LogoComment from "../../../assets/svg/Home/comentarios.svg";
import LogoShare from "../../../assets/svg/Home/compartir.svg";
import LogoPoint from "../../../assets/svg/Home/tresPuntos.svg";

const Publicaciones = ({ email, username, avatar, post, text }) => {
  const navigation = useNavigation();
  const [comment, setComment] = React.useState("");

  const userId = SecureStore.getItemAsync("userId");
  
  console.log("UserId: " + userId);

  async function CommentFunction() {
    console.log("Comment: " + comment);
    let data = new FormData();
    data.append("user_id", 1);
    data.append("post_id", 5);
    data.append("text", comment);

    const res = fetch("https://slogan.com.bo/blssd/postsComments/addMobile", {
      method: "POST",
      body: data,
    });

    console.log("Response: " + res);
    const json = await res.json();
    console.log("JSON: " + json);

    if (json.response.status) {
      Alert.alert("Like");
    } else {
      Alert.alert("error");
    }
  }

  async function LikeFunction() {
    let data = new FormData();
    data.append("user_id", 1);
    data.append("post_id", 5);

    const res = fetch("https://slogan.com.bo/blssd/postsLikes/addMobile", {
      method: "POST",
      body: data,
    });

    console.log("Response: " + res);
    const json = await res.json();
    console.log("JSON: " + json);

    if (json.response.status) {
      Alert.alert("Like");
    } else {
      Alert.alert("error");
    }
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <View style={styles.publication}>
          <View style={styles.header}>
            <View style={styles.userImageContainer}>
              <Image style={styles.userImage} source={{ uri: avatar }} />
            </View>
            <View style={styles.userData}>
              <Text style={styles.userName}>{username}</Text>
              <Text style={styles.userMail}>{email}</Text>
            </View>
            <View style={styles.sideButtonContainer}>
              <TouchableOpacity
                style={styles.sideButton}
                onPress={() => {
                  alert("Side button pressed");
                }}
              >
                <Text style={styles.sideButtonText}>Genesis Cochabamba</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.post}>
            <Image style={styles.imagePost} source={{ uri: post }} />
          </View>

          <View style={styles.footer}>
            <View style={styles.interaction}>
              <TouchableOpacity
                style={styles.interactionButton}
                onPress={() => {
                  LikeFunction();
                }}
              >
                <LogoLove style={styles.imageInteraction} />
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.interactionButton}
                onPress={() => {
                  alert("Comment");
                }}
              >
                <LogoComment style={styles.imageInteraction} />
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.interactionButton}
                onPress={() => {
                  alert("Share");
                }}
              >
                <LogoShare style={styles.imageInteraction} />
              </TouchableOpacity>
            </View>

            <View style={styles.more}>
              <TouchableOpacity
                style={styles.moreButton}
                onPress={() => {
                  alert("More options");
                }}
              >
                <LogoPoint style={styles.moreImage} />
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.postStats}>
            <View style={styles.blessingsContainer}>
              <Text style={styles.blessings}>12 blessings</Text>
            </View>
            <View style={styles.description}>
              <Text style={styles.descriptionText}>
                <Text style={styles.descriptionUsername}>
                  {username} &nbsp;
                </Text>
                {text}
              </Text>
            </View>
            <View style={styles.totalCommentsContainer}>
              <Text
                style={styles.totalComments}
                onPress={() => {
                  navigation.navigate("Comments");
                }}
              >
                Ver todos los 24 comentarios
              </Text>
            </View>
          </View>

          <View style={styles.comments}>
            <View style={styles.userAvatarContainer}>
              <Image
                style={styles.userAvatarComment}
                source={{ uri: avatar }}
              />
            </View>
            <View style={styles.userComment}>
              <TextInput
                style={styles.commentInput}
                editable
                placeholder="Add a comment..."
                onChangeText={(text) => setComment(text)}
                value={comment}
              />
            </View>
            <View style={styles.sendComment}>
              <TouchableOpacity
                style={styles.sendCommentButton}
                onPress={() => {
                  CommentFunction();
                }}
              ></TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  publication: {
    width: "100%",
    height: vh(70),
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "5%",
  },
  header: {
    height: "10%",
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  userImageContainer: {
    width: "20%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  userImage: {
    width: 50,
    height: 50,
    resizeMode: "contain",
    borderRadius: 50,
    overflow: "hidden",
  },
  userData: {
    width: "50%",
    height: "100%",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "center",
  },
  userName: {
    fontSize: 17,
    fontWeight: "bold",
  },
  userMail: {
    fontSize: 15,
  },
  sideButtonContainer: {
    width: "30%",
    height: "100%",
    alignItems: "flex-start",
    justifyContent: "center",
  },
  sideButton: {
    width: "90%",
    height: "50%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "blue",
    borderRadius: 5,
  },
  sideButtonText: {
    fontSize: 10,
    color: "white",
  },
  post: {
    width: vw(100),
    height: "65%",
    justifyContent: "center",
    alignItems: "center",
  },
  imagePost: {
    width: vw(100),
    height: "100%",
    resizeMode: "cover",
  },
  footer: {
    width: "100%",
    height: "9%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  interaction: {
    width: "40%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 20,
  },
  interactionButton: {
    width: "25%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  imageInteraction: {
    width: "100%",
    height: "100%",
    resizeMode: "contain",
  },
  more: {
    width: "20%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  moreButton: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  moreImage: {
    width: "100%",
    height: "100%",
    resizeMode: "contain",
  },
  postStats: {
    width: "100%",
    height: "11%",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "space-evenly",
    paddingHorizontal: 20,
  },
  blessingsContainer: {
    width: "100%",
    height: "30%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  blessings: {
    fontSize: 15,
    fontWeight: "bold",
  },
  description: {
    width: "100%",
    height: "auto",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    marginVertical: 5,
  },
  descriptionUsername: {
    fontSize: 16,
    fontWeight: "bold",
  },
  descriptionText: {
    fontSize: 16,
  },
  totalCommentsContainer: {
    width: "100%",
    height: "30%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  totalComments: {
    fontSize: 15,
  },
  comments: {
    width: "100%",
    height: "9%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    paddingHorizontal: 20,
  },
  userAvatarContainer: {
    width: "20%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  userAvatarComment: {
    width: 40,
    height: 40,
    resizeMode: "contain",
    borderRadius: 50,
  },
  userComment: {
    width: "60%",
    height: "60%",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "space-evenly",
  },
  commentInput: {
    width: "100%",
    height: "100%",
    borderWidth: 1,
    borderColor: "blue",
    borderRadius: 10,
    paddingHorizontal: 10,
    fontSize: 15,
  },
  sendComment: {
    width: "10%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  sendCommentButton: {
    width: "80%",
    height: "60%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "blue",
    borderRadius: 20,
  },
});

export default Publicaciones;
