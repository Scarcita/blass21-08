import React, { useEffect, useState } from "react";
import { FlatList, View, StyleSheet } from "react-native";
import HomeStories from "./HomeStories";
import Publicaciones from "./Publicaciones";
import PublicacionesDos from "./PublicacionesDos";
import PublicacionesTres from "./PublicacionesTres";

const Body = (update) => {
  const [data, setData] = useState([]);

  const getUsuarios = async () => {
    try {
      const res = await fetch("https://slogan.com.bo/blssd/posts/feed");
      const json = await res.json();
      setData(Object.values(json.response.data));
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getUsuarios();
  }, [update]);

  return (
    <View style={bodyStyles.container}>
      <View
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        style={bodyStyles.scrollView}
      >
        <View style={bodyStyles.historias}>
          <FlatList
            style={bodyStyles.flatListStories}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={data}
            keyExtractor={({ id }, index) => id}
            renderItem={({ item }) => (
              <HomeStories avatar={item.user.profile_img}></HomeStories>
            )}
          />
        </View>

        <View style={bodyStyles.publicaciones}>
          <FlatList
            style={bodyStyles.flatListPublicaciones}
            data={data}
            keyExtractor={({ id }, index) => id}
            renderItem={
              ({ item }) =>
                item.type == "image" ? (
                  <Publicaciones
                    email={item.user.email}
                    username={item.user.username}
                    avatar={item.user.profile_img}
                    post={item.media_url}
                    text={item.text}
                  ></Publicaciones>
                ) : (
                  <PublicacionesDos
                    email={item.user.email}
                    username={item.user.username}
                    avatar={item.user.profile_img}
                    post={item.media_url}
                    text={item.text}
                  ></PublicacionesDos>
                )

              // <PublicacionesDos
              //   email={item.email}
              //   username={item.username}
              //   avatar={item.avatar}
              // ></PublicacionesDos>

              // <PublicacionesTres
              //   email={item.email}
              //   username={item.username}
              //   avatar={item.avatar}
              // ></PublicacionesTres>

              // <PublicacionesCuatro email={item.email} username={item.username} avatar={item.avatar} ></PublicacionesCuatro>
            }
          />
        </View>
      </View>
    </View>
  );
};

export default Body;

const bodyStyles = StyleSheet.create({
  container: {
    flex: 1,
    overflow: "scroll",
  },
  scrollView: {
    width: "100%",
    height: "100%",
    overflow: "scroll",
  },
  historias: {
    width: "100%",
    height: 100,
    alignItems: "center",
    justifyContent: "center",
    overflow: "scroll",
  },
  flatListStories: {
    width: "100%",
    height: "100%",
  },
  publicaciones: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  flatListPublicaciones: {
    width: "100%",
    height: "100%",
  },
});
