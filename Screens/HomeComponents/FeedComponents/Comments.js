import React from "react";
import { useState } from "react";
import { StyleSheet, Text, View, Image, FlatList } from "react-native";
import { useEffect } from "react";
import { Feather } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";

import LogoLove from "../../../assets/svg/Home/corazonComment.svg";
import LogoAnswer from "../../../assets/svg/Home/answerComment.svg";
import LogoComentar from "../../../assets/svg/Home/compartirComment.svg";

// COMPONENTS
const Comment = ({ avatar, first_name }) => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={notificacionStyles.notificaciones}>
        <View style={notificacionStyles.imagen}>
          <Image source={{ uri: avatar }} style={notificacionStyles.avatar} />
        </View>

        <View style={notificacionStyles.textos}>
          <View style={notificacionStyles.header}>
            <Text style={notificacionStyles.textoUsuario}>{first_name}</Text>

            <Text style={notificacionStyles.textoHora}> Hace 5 horas</Text>
          </View>
          <View>
            <Text>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu
              ex diam.
            </Text>
          </View>
          <View style={notificacionStyles.interaccion}>
            <LogoLove style={notificacionStyles.reacionesComentarios} />
            <LogoAnswer style={notificacionStyles.reacionesComentarios} />
            <LogoComentar style={notificacionStyles.reacionesComentarios} />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};
// COMPONENT END

// COMPONENT STYLES
const notificacionStyles = StyleSheet.create({
  notificaciones: {
    width: "100%",
    height: 120,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    marginVertical: "2%",
  },
  imagen: {
    width: 70,
    height: 70,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: "5%",
  },
  avatar: {
    width: "100%",
    height: "100%",
    borderRadius: 50,
  },
  textos: {
    width: "70%",
    height: "100%",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "center",
  },
  header: {
    height: "20%",
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  textoUsuario: {
    fontSize: 16,
    fontWeight: "bold",
  },
  textoHora: {
    color: "#8E8E8E",
    fontSize: 12,
  },
  interaccion: {
    width: "50%",
    height: "15%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    marginTop: "5%",
  },
  reacionesComentarios: {
    height: 20,
    width: 20,
    marginRight: 10,
    resizeMode: "contain",
  },
});

// MAIN COMPONENT
const Comments = () => {
  const navigation = useNavigation();
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getUsuarios = async () => {
    try {
      const response = await fetch("https://reqres.in/api/users?page=1");
      const json = await response.json();
      setData(json.data);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getUsuarios();
  }, []);

  return (
    <View style={mainStyles.container}>
      <View style={mainStyles.header}>
        <View style={mainStyles.iconoContainer}>
          <Feather
            name="arrow-left"
            style={mainStyles.icono}
            onPress={() => {
              navigation.goBack();
            }}
          />
        </View>
        <View style={mainStyles.tituloContainer}>
          <Text style={mainStyles.titulo}>Comments</Text>
        </View>
      </View>

      <View style={mainStyles.contenedor}>
        <FlatList
          data={data}
          keyExtractor={({ id }, index) => id}
          renderItem={({ item }) => (
            <Comment
              avatar={item.avatar}
              first_name={item.first_name}
            ></Comment>
          )}
        />
      </View>
    </View>
  );
};

const mainStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
  },
  header: {
    flexDirection: "row",
    width: "100%",
    height: "10%",
    justifyContent: "flex-start",
    alignItems: "center",
    textAlign: "center",
  },
  iconoContainer: {
    width: "10%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: "5%",
  },
  icono: {
    fontSize: 30,
    color: "#0435F0",
  },
  tituloContainer: {
    width: "80%",
    height: "100%",
    justifyContent: "center",
    alignItems: "flex-start",
  },
  titulo: {
    fontWeight: "bold",
    fontSize: 20,
  },
  contenedor: {
    width: "90%",
    height: "90%",
    overflow: "scroll",
  },
});

export default Comments;
