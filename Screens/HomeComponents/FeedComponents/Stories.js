import React from "react";
import { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";

import { useNavigation } from "@react-navigation/native";

const Historia = ({ avatar, first_name, email }) => {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={storyStyles.contenedor}>
        {/* HISTORIAS CAJA GRANDE */}
        <View style={storyStyles.contenedorHistoria}>
          <TouchableOpacity
            style={storyStyles.contenedorHistoriaBoton}
            onPress={() => {
              navigation.navigate("StorieVideo");
            }}
          >
            <Image source={{ uri: avatar }} style={storyStyles.storyImage} />
          </TouchableOpacity>
        </View>

        {/* // ESTILOS DE LOS PERFIL DE LAS HISTORIAS CAJA PEQUEÑA */}

        <View style={storyStyles.contenedorUsuario}>
          <TouchableOpacity
            style={storyStyles.contenedorAvatarUsuario}
            onPress={() => {
              navigation.navigate("Profile");
            }}
          >
            <View style={storyStyles.contenedorAvatar}>
              <View style={storyStyles.historiaDisponible}>
                <View style={storyStyles.contenedorImagen}>
                  <Image
                    source={{ uri: avatar }}
                    style={storyStyles.avatarImage}
                  />
                </View>
              </View>
            </View>

            <View style={storyStyles.contenedorTexto}>
              <Text style={storyStyles.textoBlack}>{first_name}</Text>
              <Text style={storyStyles.textoGray}> {email}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const storyStyles = StyleSheet.create({
  contenedor: {
    width: 150,
    height: 250,
    margin: 20,
    alignItems: "center",
    justifyContent: "space-between",
  },
  //  ESTILOS DE LAS HISTORIAS CAJA GRANDE
  contenedorHistoria: {
    width: "100%",
    height: "100%",
  },
  contenedorHistoriaBoton: {
    width: "100%",
    height: "100%",
  },
  storyImage: {
    width: "100%",
    height: "100%",
    borderRadius: 10,
    opacity: 0.6,
  },
  // ESTILOS DE LOS PERFIL DE LAS HISTORIAS CAJA PEQUEÑA
  contenedorUsuario: {
    width: "100%",
    height: "20%",
    position: "absolute",
    bottom: 10,
  },
  contenedorAvatarUsuario: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  contenedorAvatar: {
    width: "30%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  historiaDisponible: {
    width: 40,
    height: 40,
    borderRadius: 50,
    zIndex: 10,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#0f49f2",
  },
  contenedorImagen: {
    width: 35,
    height: 35,
    borderRadius: 50,
    zIndex: 15,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#909090",
  },
  avatarImage: {
    width: 30,
    height: 30,
    borderRadius: 50,
    zIndex: 20,
  },
  contenedorTexto: {
    width: "70%",
    height: "100%",
    flexDirection: "column",
    justifyContent: "flex-end",
    alignContent: "center",
  },
  textoBlack: {
    width: "100%",
    height: "30%",
    color: "black",
    fontWeight: "bold",
    bottom: 0,
  },
  textoGray: {
    width: "100%",
    height: "30%",
    color: "#201917",
    fontSize: 7.5,
    top: 0,
  },
});

// COMPONENTS
const Stories = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getUsuarios = async () => {
    try {
      const response = await fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      setData(json.data);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getUsuarios();
  }, []);

  // const renderItem = ({ item }) => (
  //     <Item title={item.title} />
  // );
  return (
    <View style={styles.cajagrande}>
      {/* HEADER */}
      <View style={styles.header}>
        <Text style={styles.texto}>Nuevas</Text>
        <Text style={styles.texto}>Historias</Text>
      </View>
      {/* LAS HISTORIAS */}
      <View style={styles.contenedor}>
        <View style={styles.historias}>
          <FlatList
            numColumns={2}
            data={data}
            keyExtractor={({ id }, index) => id}
            renderItem={({ item }) => (
              <Historia
                avatar={item.avatar}
                first_name={item.first_name}
                email={item.email}
              ></Historia>
            )}
          />
        </View>
      </View>
    </View>
  );
};

// COMPONENT STYLES
const styles = StyleSheet.create({
  cajagrande: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  header: {
    flexDirection: "column",
    width: "100%",
    height: "15%",
    justifyContent: "flex-end",
    paddingHorizontal: "10%",
  },
  texto: {
    fontSize: 30,
    fontWeight: "bold",
  },
  contenedor: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
});

export default Stories;
