import React from "react";
import { View, StyleSheet, Image } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";

const HomeStories = ({ avatar }) => {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.contenedor}>
        <View style={styles.nuevaHistoria}>
          <View style={styles.contenedorImagen}>
            <Image
              style={styles.imagen}
              source={{ uri: avatar }}
              onPress={() => {
                navigation.navigate("StorieVideo");
              }}
            ></Image>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  contenedor: {
    width: 100,
    height: 100,
    alignItems: "center",
    justifyContent: "center",
  },
  nuevaHistoria: {
    width: 90,
    height: 90,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 50,
    backgroundColor: "purple",
  },
  contenedorImagen: {
    width: 85,
    height: 85,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 50,
    backgroundColor: "white",
  },
  imagen: {
    width: 80,
    height: 80,
    borderRadius: 50,
  },
});

export default HomeStories;
