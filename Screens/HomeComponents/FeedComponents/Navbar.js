import React from "react";
import { View, StyleSheet, Image, TouchableOpacity } from "react-native";

import { useNavigation } from "@react-navigation/native";
import LogoHome from "../../../assets/svg/Home/home.svg";
import LogoSearch from "../../../assets/svg/Home/lupa.svg";
import LogoUpload from "../../../assets/svg/Home/camara.svg";
import LogoComment from "../../../assets/svg/Home/comentarios.svg";
import LogoProfile from "../../../assets/svg/Home/perfil.svg";

const Navbar = () => {
  const navigation = useNavigation();

  return (
    <View style={navbarStyles.navbar}>
      <TouchableOpacity
        style={navbarStyles.navbarButton}
        onPress={() => {
          navigation.navigate("Feed");
        }}
      >
        <LogoHome style={navbarStyles.navbarImage} />
      </TouchableOpacity>

      <TouchableOpacity
        style={navbarStyles.navbarButton}
        onPress={() => {
          alert("se ha iniciado");
        }}
      >
        <LogoSearch style={navbarStyles.navbarImage} />
      </TouchableOpacity>
      <TouchableOpacity
        style={navbarStyles.navbarButton}
        onPress={() => {
          navigation.navigate("Camera");
        }}
      >
        <LogoUpload style={navbarStyles.navbarImage} />
      </TouchableOpacity>

      <TouchableOpacity
        style={navbarStyles.navbarButton}
        onPress={() => {
          alert("se ha iniciado");
        }}
      >
        <LogoComment style={navbarStyles.navbarImage} />
      </TouchableOpacity>

      <TouchableOpacity
        style={navbarStyles.navbarButton}
        onPress={() => {
          navigation.navigate("Profile");
        }}
      >
        <LogoProfile style={navbarStyles.navbarImage} />
      </TouchableOpacity>
    </View>
  );
};

const navbarStyles = StyleSheet.create({
  navbar: {
    height: "6%",
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  navbarButton: {
    height: "100%",
    width: "10%",
    alignItems: "center",
    justifyContent: "center",
  },
  navbarImage: {
    width: "70%",
    height: "70%",
    resizeMode: "contain",
  },
});

export default Navbar;
