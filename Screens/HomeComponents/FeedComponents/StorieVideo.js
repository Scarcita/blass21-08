import React from "react";
import { View, StyleSheet } from "react-native";

import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";

import { Audio, Video } from "expo-av";
import { Feather } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";

const StorieVideo = () => {
  const video = React.useRef(null);
  const [status, setStatus] = React.useState({});

  const navigation = useNavigation();
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <Feather
          name="play"
          size={24}
          color="black"
          style={styles.goBack}
          onPress={() => {
            navigation.goBack();
          }}
        />
        <Video
          ref={video}
          style={styles.video}
          source={require("../../../assets/img/video-de-prueba.mp4")}
          useNativeControls
          isLooping
          onPlaybackStatusUpdate={(status) => setStatus(() => status)}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  goBack: {
    position: "absolute",
    top: 40,
    left: 30,
  },
  video: {
    flex: 1,
    resizeMode: "contain",
  },
});

export default StorieVideo;
