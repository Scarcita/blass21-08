import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Image, TextInput } from 'react-native';


const PublicacionesCuatro = ({ email, first_name, avatar }) => {

  return (
    <View style={styles.publicaciones}>

      <View style={styles.cajaGrande}>
        <View style={styles.publicacion}>
          <View style={styles.superior}>
            <View style={styles.UsuarioPublicacion}>

              <View style={styles.textoUsuario}>
                <Text style={{ fontWeight: 'bold' }}>{first_name}</Text>
                <Text>{email}</Text>

                <Image style={{
                  width: 250,
                  height: 250,
                }}
                  source={{uri:avatar}}>
                </Image>
              </View>
            </View>

            {/* <Button
            title="Boton azul"
            onPress={() => Alert.alert('Button with adjusted color pressed')}
          /> */}
          </View>

          <View style={styles.inferior}>
            <View style={styles.interaccion}>
              <Image
                style={{ width: 30, height: 50, resizeMode: 'contain' }}
                source={require("../../../assets/img/Home/love.png")}
              />

              <Image
                style={{ width: 30, height: 50, resizeMode: 'contain' }}
                source={require("../../../assets/img/Home/comment.png")}
              />

              <Image
                style={{ width: 30, height: 50, resizeMode: 'contain' }}
                source={require("../../../assets/img/Home/share.png")}
              />
            </View>

            <View style={styles.los3puntos}>

              <TouchableOpacity
                onPress={() => {
                  alert("se ha iniciado");
                }}
              >
                <Image
                  style={{ width: 30, height: 50, resizeMode: 'contain' }}
                  source={require("../../../assets/img/Home/point.jpg")}
                />
              </TouchableOpacity>


            </View>

          </View>

          <View style={styles.comentarios}>

            <View style={styles.contadorReacciones}>
              <Text style={{ fontWeight: 'bold' }}>2,003 likes</Text>
              <Text>Ver todos los 24 comentarios</Text>

            </View>

            <View style={styles.cajacomentario}>

              <Image style={{
                width: 40,
                height: 40,
                borderRadius: 50,
              }}
                source={{uri:avatar}}>
              </Image>

              <TextInput
                style={{
                  width: 200,
                  height: "100%",
                  // borderColor: "blue",
                  // borderWidth: 1,
                  borderRadius: 10,
                  // padding: "2%",
                  // backgroundColor:'lime'
                }}
                editable

              />
            </View>

          </View>
        </View>
      </View>



    </View>
  )
}

const styles = StyleSheet.create({

  publicaciones: {
    height: 470,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
    // backgroundColor: 'red'

  },
  cajaGrande: {
    height: 480,
    width: 350,
    borderRadius: 10,
    alignItems: 'center',
  },
  publicacion: {
    height: 420,
    width: 300,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  publicaciondeUsuario: {
    width: 100,
    height: 200,

  },
  superior: {
    height: 300,
    width: '90%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // backgroundColor:'powderblue'
  },
  textoUsuario: {
    height: 10,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    // backgroundColor:'magenta'
  },

  interaccion: {
    height: 20,
    width: 150,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    // backgroundColor:'lime'


  },

  inferior: {
    height: 80,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',

  },

  comentarios: {
    height: 10,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-around',

  },

  contadorReacciones: {
    height: 40,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-around',

  },

  cajacomentario: {
    height: 40,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

});

export default PublicacionesCuatro;
