import React from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import LogoTextHeader from "../../../assets/svg/Home/textHeader";
import LogoIcono from "../../../assets/svg/Home/iconoHeader";

import { Context } from "../../../context/AuthContext";

import { Provider as AuthProvider } from "../../../context/AuthContext";
import { Context as AuthContext } from "../../../context/AuthContext";

const Header = () => {
  const navigation = useNavigation();
  const { signOut } = React.useContext(AuthContext);
  return (
    <View style={headerStyles.container}>
      <View style={headerStyles.leftButtons}>
        <View style={headerStyles.logoContainer}>
          <LogoTextHeader style={headerStyles.logoImage} />
        </View>
        <View style={headerStyles.menuIconContainer}>
          <Ionicons
            name="chevron-down"
            size={40}
            style={headerStyles.menuIcon}
            onPress={() => {
              navigation.navigate("Stories");
            }}
          />
        </View>
      </View>
      <View style={headerStyles.rightButtons}>
        <View style={headerStyles.buttonContainer}>
          <TouchableOpacity
            style={headerStyles.buttonSurface}
            onPress={() => {
              navigation.navigate("Notifications");
            }}
          >
            <LogoIcono style={headerStyles.buttonImage} />
          </TouchableOpacity>
        </View>
        <View style={headerStyles.buttonContainer}>
          <TouchableOpacity
            style={headerStyles.buttonSurface}
            onPress={() => {
              signOut();
            }}
          >
            <Image
              style={headerStyles.buttonImage}
              source={require("../../../assets/img/Home/heart.png")}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const headerStyles = StyleSheet.create({
  container: {
    height: "10%",
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 10,
  },
  leftButtons: {
    height: "100%",
    width: "60%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  logoContainer: {
    height: "100%",
    width: "70%",
    alignItems: "center",
    justifyContent: "center",
  },
  logoImage: {
    height: "100%",
    width: "100%",
    resizeMode: "contain",
  },
  menuIconContainer: {
    height: "100%",
    width: "20%",
    alignItems: "center",
    justifyContent: "center",
  },
  menuIcon: {
    color: "blue",
  },
  rightButtons: {
    height: "80%",
    width: "30%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  buttonContainer: {
    height: "100%",
    width: "25%",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonSurface: {
    height: "100%",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonImage: {
    height: "100%",
    width: "100%",
    resizeMode: "contain",
  },
});

export default Header;
