import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
} from "react-native";
import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";

import { Feather } from "@expo/vector-icons";

import { useNavigation } from "@react-navigation/native";

function NewPost(props) {
  const [description, setDescription] = useState("");

  const navigation = useNavigation();

  const cameraResult = props.route.params.source;

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={newPostStyles.container}>
        {/* HEADER */}
        <View style={newPostStyles.header}>
          <View style={newPostStyles.iconContainer}>
            <Feather
              name="arrow-left"
              style={newPostStyles.icon}
              onPress={() => {
                navigation.goBack();
              }}
            />
          </View>
          <View style={newPostStyles.titleContainer}>
            <Text style={newPostStyles.title}>New Post</Text>
          </View>
        </View>

        <View style={newPostStyles.userContainer}>
          <View style={newPostStyles.userAvatarContainer}>
            <Image
              style={newPostStyles.userAvatar}
              source={require("../../../assets/img/Home/profile_pic.png")}
            ></Image>
          </View>
          <View style={newPostStyles.userName}>
            <Text style={newPostStyles.userNameText}>Jordan Peterson</Text>
          </View>
        </View>

        <View style={newPostStyles.post}>
          <Image
            style={newPostStyles.postImage}
            source={{ uri: cameraResult }}
          />
        </View>

        <View style={newPostStyles.description}>
          <TextInput
            style={newPostStyles.descriptionInput}
            placeholder="Describe your post"
            onChangeText={(text) => setDescription(text)}
            value={description}
          />
        </View>

        <View style={newPostStyles.buttonContainer}>
          <TouchableOpacity
            style={newPostStyles.button}
            onPress={async () => {
              navigation.navigate({
                name: "Feed",
                params: {
                  isLoading: true,
                  cameraShoot: cameraResult,
                  description,
                },
                merge: true,
              });
            }}
          >
            <Text style={newPostStyles.buttonText}>Post</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
}

const newPostStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
  },
  header: {
    flexDirection: "row",
    width: "90%",
    height: "10%",
    justifyContent: "flex-start",
    alignItems: "center",
    textAlign: "center",
    marginHorizontal: "5%",
  },
  iconContainer: {
    width: "10%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  icon: {
    fontSize: 30,
    color: "#0435F0",
  },
  titleContainer: {
    width: "80%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  userContainer: {
    flexDirection: "row",
    width: "90%",
    height: "10%",
    justifyContent: "flex-start",
    alignItems: "center",
    textAlign: "center",
    marginHorizontal: "5%",
  },
  userAvatarContainer: {
    width: "20%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginRight: "5%",
  },
  userAvatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  userName: {
    width: "80%",
    height: "100%",
    justifyContent: "center",
    alignItems: "flex-start",
  },
  userNameText: {
    fontSize: 20,
    fontWeight: "bold",
  },
  post: {
    width: "100%",
    height: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  postImage: {
    width: "100%",
    height: "100%",
    resizeMode: "contain",
  },
  description: {
    width: "100%",
    height: "10%",
    justifyContent: "center",
    alignItems: "center",
  },
  descriptionInput: {
    width: "90%",
    height: "70%",
    borderColor: "#0435F0",
    borderWidth: 1.5,
    borderRadius: 10,
    padding: 10,
  },
  buttonContainer: {
    width: "90%",
    height: "7%",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "5%",
  },
  button: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#0435F0",
    borderRadius: 10,
  },
  buttonText: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#fff",
  },
});

export default NewPost;
