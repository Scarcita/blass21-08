import React from "react";
import { useState, useEffect } from "react";
import * as SecureStore from "expo-secure-store";


////////IMAGENES//////
import ImgProfile from '../../assets/img/Home/perfil.svg'
import ImgVerificacion from '../../assets/img/Home/verificacion.svg';
import ImgPunto from '../../assets/img/Home/punto.svg';
import ImgEdit from '../../assets/img/Home/editProfile';
import ImgUpload from '../../assets/img/Home/upload';
import ImgCudrado from '../../assets/img/Home/cudrado.svg';
import ImgCorazon from '../../assets/img/Home/corazon.svg';
import ImgPencil from '../../assets/img/Home/pencilprofile.svg';
import ImgLove from '../../assets/img/Home/loveDashboard.svg'

import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  Dimensions,
  TouchableOpacity,
} from "react-native";

import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";

import { vw, vh } from "react-native-expo-viewport-units";

import { useNavigation } from "@react-navigation/native";

import { TabView, SceneMap } from "react-native-tab-view";

import { BarChart } from "react-native-chart-kit";

const screenWidth = Dimensions.get("window").width;

const initialLayout = { width: Dimensions.get("window").width };

// COMPONENTS

const PostsGrid = ({ avatar }) => {
  return <Image source={{ uri: avatar }} style={componentStyles.avatar} />;
};

function FirstRoute() {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getUsuarios = async () => {
    try {
      const response = await fetch("https://reqres.in/api/users?page=1");
      const json = await response.json();
      setData(json.data);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getUsuarios();
  }, []);

  return (
    <View style={componentStyles.bodyContainerPostsContainer}>
      <FlatList
        numColumns={3}
        horizontal={false}
        data={data}
        keyExtractor={({ id }, index) => id}
        renderItem={({ item }) => <PostsGrid avatar={item.avatar}></PostsGrid>}
      />
    </View>
  );
}

function SecondRoute() {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getUsuarios = async () => {
    try {
      const response = await fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      setData(json.data);
    } catch (error) {
      console.error(error);barChar
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getUsuarios();
  }, []);

  return (
    <View style={componentStyles.bodyContainerPostsContainer}>
      <FlatList
        numColumns={3}
        horizontal={false}
        data={data}
        keyExtractor={({ id }, index) => id}
        renderItem={({ item }) => <PostsGrid avatar={item.avatar}></PostsGrid>}
      />
    </View>
  );
}

function ThirdRoute() {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getUsuarios = async () => {
    try {
      const response = await fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      setData(json.data);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getUsuarios();
  }, []);

  return (
    <View style={componentStyles.bodyContainerPostsContainer}>
      <FlatList
        numColumns={3}
        horizontal={false}
        data={data}
        keyExtractor={({ id }, index) => id}
        renderItem={({ item }) => <PostsGrid avatar={item.avatar}></PostsGrid>}
      />
    </View>
  );
}

function FourthRoute() {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getUsuarios = async () => {
    try {
      const response = await fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      setData(json.data);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getUsuarios();
  }, []);

  const dataDiagram = {
    labels: [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ],
    datasets: [
      {
        data: [20, 45, 28, 50, 10, 43, 25, 45, 35, 60, 35, 50],
      },
    ],
  };

  const chartConfig = {
    backgroundGradientFrom: "white",
    backgroundGradientFromOpacity: 1,
    backgroundGradientTo: "white",
    //backgroundGradientToOpacity: 1,
    color: (opacity = 1) => `rgb(0, 0, 255, ${opacity})`,
    //strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
  };

  return (
    <View style={fourthTabStyles.container}>
      {/* TITULO */}
      <View style={fourthTabStyles.tituloPrincipal}>
        <Text style={fourthTabStyles.titulo}>Dashboard</Text>
      </View>

      {/* DONACIONES DEL AÑO */}
      <View style={fourthTabStyles.graficos}>
        <View style={{position: 'absolute', marginLeft: 4,marginTop: 4}}>
        <BarChart
            style={fourthTabStyles.barChart}
            data={dataDiagram}
            width={325}
            height={210}
            //yAxisLabel="$"
            chartConfig={chartConfig}
            //verticalLabelRotation={30}
          />
          </View>
        <View style={fourthTabStyles.montorecaudado}>
          <ImgLove style={{marginRight: 10}}/>
          <View>
          <Text style={fourthTabStyles.textoMoneda}>$874.743</Text>
          <Text style={fourthTabStyles.descricion}>Total donado en el año</Text>
          </View>
        </View>
      </View>

      {/* CAUSAS Y PAISES QUE DONARON */}
      {/* <View style={fourthTabStyles.donaciones}>
        <View style={fourthTabStyles.causasdeonadas}>
          <Text style={fourthTabStyles.numero}>36</Text>
          <Text>Causas Apoyadas</Text>
        </View>
        <View style={fourthTabStyles.paisesdonados}>
          <Image
            source={require("../../assets/img/Vector.png")}
            style={fourthTabStyles.emojiCorazon}
          />
          <Text style={fourthTabStyles.numero}>12</Text>
          <Text>Países apoyados</Text>
        </View>
      </View> */}

      {/* ULTIMAS DONACIONES */}

    </View>
  );
}
const fourthTabStyles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
  },
  tituloPrincipal: {
    //flex: 0.5,
    marginTop:15,
    justifyContent: "center",
    alignItems: "center",
  },
  titulo: {
    fontWeight: "bold",
    fontSize: 24,
  },

  graficos: {
    width: 334,
    height: 220,
    marginTop: 24,
    borderWidth: 1,
    borderColor: '#0435F0',
    alignSelf: 'center',
    borderRadius: 6,
  },

  montorecaudado: {
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: "center",
    position: 'absolute',
    marginLeft: 100,
    marginTop: 15
  },
  textoMoneda: {
    color: "#0435F0",
    fontWeight: "bold",
    fontSize:24,
  },
  descricion: {
    color: '#9C9C9C',
    fontSize: 10
  },
  donaciones: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  causasdeonadas: {
    flex: 1,
    flexDirection: "row",
  },
  paisesdonados: {
    flex: 1,
    flexDirection: "row",
  },
  numero: {
    fontWeight: "bold",
  },
  donacionesUltimas: {
    width: "100%",
    height: "48.5%",
  },
  tituloCausas: {
    flex: 0.5,
    // textAlign:'center',
    paddingLeft: 50,
    fontWeight: "bold",
    fontSize: 20,
    justifyContent: "center",
  },
  imagenesComentario: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderRadius: 10,
  },
});



const renderScene = SceneMap({
  one: FirstRoute,
  two: SecondRoute,
  three: ThirdRoute,
  four: FourthRoute,
});

// COMPONENT END

// COMPONENT STYLES
const componentStyles = StyleSheet.create({
  avatar: {
    width: vw(33),
    height: vw(33),
  },
  scene: {
    flex: 1,
  },
});

// COMPONENT STYLES END

// MAIN COMPONENT
const Profile = () => {

  const username = SecureStore.getItemAsync("username");
  console.log('username', username);

  const [index, setIndex] = React.useState(0);
  const navigation = useNavigation();

  const [routes] = React.useState([
    { key: "one", title: "Posts" },
    { key: "two", title: "Donations" },
    { key: "three", title: "Stories" },
    { key: "four", title: "Dashboard" },
  ]);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={mainStyles.container}>
        <View style={mainStyles.header}>
          <View style={mainStyles.profileDetails}>
            <View style={mainStyles.profileAvatar}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("Stories");
                }}
              >
                <View style={mainStyles.profileAvatarStory}>
                  <View style={mainStyles.profileAvatarContainer}>
                    <ImgProfile
                      style={mainStyles.avatar}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={mainStyles.profileInfo}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={mainStyles.profileInfoUsername}>Crickster</Text>
                <ImgVerificacion style={{width: 26, height: 25,marginLeft: 10, marginTop: 5}}/>
              </View> 
              <Text style={{color:'8E8E8E', fontSize: 10 }}>Multimedia Ministry Leader </Text>
              <Text style={{color: '#0435F0',fontSize: 14}}>Iglesia Genesis</Text>
              
              <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={{flexDirection:'row', marginRight: 10}}>
                  <ImgPunto style= {{marginRight: 5, marginTop: 5}}/>
                  <Text style={{color: '#8E8E8E',fontSize: 10}}>Alabanzas</Text>
                </View> 
                <View style={{flexDirection:'row'}}>
                  <ImgPunto style={{marginRight:5, marginTop: 5}}/> 
                  <Text style={{color: '#8E8E8E', fontSize:10}}>Logistica</Text>
                </View> 
            </View> 



              <View style={mainStyles.profileInfoButtons}>
                <View style= {{marginRight: 5}}>
                  <ImgEdit/>
                  <Text style={{color: '#fff', fontSize:12, position: 'absolute',marginLeft: 10, marginTop: 3}}>Edit profile</Text>
                  <ImgPencil style={{position: 'absolute', marginTop: 4, marginLeft: 70}}/>
                </View>           
                <ImgUpload style= {{marginRight: 5}}/>
                <View >
                  <ImgCudrado/>
                  <ImgCorazon style= {{position: 'absolute',marginTop: 3,marginLeft: 3}}/>
                </View> 
              </View>
            </View>
          </View>
          <View style={mainStyles.profileStats}>
            <View style={mainStyles.profileStatsItem}>
              <Text style={mainStyles.profileStatsNumber}>9,243</Text>
              <Text style={mainStyles.profileStatsText}>Followers</Text>
            </View>
            <View style={mainStyles.profileStatsItem}>
              <Text style={mainStyles.profileStatsNumber}>128</Text>
              <Text style={mainStyles.profileStatsText}>Posts</Text>
            </View>
            <View style={mainStyles.profileStatsItem}>
              <Text style={mainStyles.profileStatsNumber}>69</Text>
              <Text style={mainStyles.profileStatsText}>Causes</Text>
            </View>
          </View>
        </View>

        <View style={mainStyles.body}>
          <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
            style={mainStyles.navigationPaneContainer}
          />
        </View>
        {/* <Navbar></Navbar> */}
      </View>
    </SafeAreaView>
  );
};

// MAIN COMPONENT END

// MAIN STYLES
const mainStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  header: {
    width: "100%",
    height: "40%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 20,
  },
  profileDetails: {
    width: "100%",
    height: "30%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  profileAvatar: {
    width: "30%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  profileAvatarStory: {
    width: 100,
    height: 100,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    backgroundColor: "#AA4FFB",
    // backgroundColor: 'linear-gradient(144.07deg, #AA4FFB 5.28%, #2A3BF2 51.76%, #44ADFA 109.05%)',
  },
  profileAvatarContainer: {
    width: 95,
    height: 95,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    backgroundColor: "#fff",
  },
  avatar: {
    width: 120,
    height: 120,
    resizeMode: "contain",
    borderRadius: 50,
  },
  profileInfo: {
    width: "60%",
    height: "100%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
    paddingHorizontal: 2,
  },
  profileInfoUsername: {
    fontSize: vw(8),
    fontWeight: "bold",
    color: "#000",
  },
  profileInfoOccupation: {
    fontSize: vw(3.5),
    color: "#000",
  },
  profileInfoButtons: {
    //width: "100%",
    height: "30%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: 'flex-start',
    marginTop: 5
  },
  profileInfoButton: {
    width: "40%",
    height: "80%",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    backgroundColor: "#0134f7",
  },
  profileInfoButtonText: {
    fontSize: vw(3.5),
    color: "#fff",
    fontWeight: "bold",
  },
  profileDescription: {
    width: "100%",
    height: "30%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
    paddingHorizontal: 15,
  },
  profileDescriptionText: {
    fontSize: vw(3.5),
    color: "#000",
  },
  profileStats: {
    width: "100%",
    height: "30%",
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    paddingHorizontal: 5,
    marginTop: 27
  },
  profileStatsItem: {
    width: "30%",
    height: "100%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  profileStatsNumber: {
    fontWeight: "bold",
    fontSize: 24,
    color: "#000",
  },
  profileStatsText: {
    fontSize: 14,
    color: "#9C9C9C",
  },

  body: {
    width: "100%",
    height: "55%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  navigationPaneContainer: {
    width: "100%",
    height: "100%",
  },
});
// MAIN STYLES END

export default Profile;
