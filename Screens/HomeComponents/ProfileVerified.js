import React from "react";
import { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  Dimensions,
  TouchableOpacity,
} from "react-native";

import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";

import { useEffect } from "react";
import { vw, vh } from "react-native-expo-viewport-units";

import { useNavigation } from "@react-navigation/native";

import { TabView, SceneMap } from "react-native-tab-view";

import { AntDesign } from "@expo/vector-icons";

import { BarChart } from "react-native-chart-kit";

const screenWidth = Dimensions.get("window").width;

const initialLayout = { width: Dimensions.get("window").width };

// COMPONENTS

const PostsGrid = ({ avatar }) => {
  return <Image source={avatar} style={componentStyles.avatar} />;
};

function FirstRoute() {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getUsuarios = async () => {
    try {
      const response = await fetch("https://reqres.in/api/users?page=1");
      const json = await response.json();
      setData(json.data);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getUsuarios();
  }, []);

  return (
    <View style={componentStyles.bodyContainerPostsContainer}>
      <FlatList
        numColumns={3}
        horizontal={false}
        data={data}
        keyExtractor={({ id }, index) => id}
        renderItem={({ item }) => <PostsGrid avatar={item.avatar}></PostsGrid>}
      />
    </View>
  );
}

function SecondRoute() {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getUsuarios = async () => {
    try {
      const response = await fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      setData(json.data);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getUsuarios();
  }, []);

  return (
    <View style={componentStyles.bodyContainerPostsContainer}>
      <FlatList
        numColumns={3}
        horizontal={false}
        data={data}
        keyExtractor={({ id }, index) => id}
        renderItem={({ item }) => <PostsGrid avatar={item.avatar}></PostsGrid>}
      />
    </View>
  );
}

function TerceroRoute() {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getUsuarios = async () => {
    try {
      const response = await fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      setData(json.data);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getUsuarios();
  }, []);

  const dataDiagram = {
    labels: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ],
    datasets: [
      {
        data: [20, 45, 28, 80, 99, 43, 25, 45, 35, 80, 85, 50],
      },
    ],
  };

  const chartConfig = {
    backgroundGradientFrom: "white",
    backgroundGradientFromOpacity: 1,
    backgroundGradientTo: "white",
    backgroundGradientToOpacity: 1,
    color: (opacity = 1) => `rgb(0, 0, 255, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
  };
  return (
    <View style={fourthTabStyles.container}>
      {/* TITULO */}
      <View style={fourthTabStyles.tituloPrincipal}>
        <Text style={fourthTabStyles.titulo}>Dashboard</Text>
      </View>

      {/* DONACIONES DEL AÑO */}
      <View style={fourthTabStyles.diagrama}>
        <View style={fourthTabStyles.montorecaudado}>
          <Text style={fourthTabStyles.textoMoneda}>$874.743</Text>
          <Text style={fourthTabStyles.descricion}>Total donado en el año</Text>
        </View>

        <BarChart
          style={fourthTabStyles.barChart}
          data={dataDiagram}
          width={screenWidth}
          height={220}
          yAxisLabel="$"
          chartConfig={chartConfig}
          verticalLabelRotation={30}
        />
      </View>

      {/* CAUSAS Y PAISES QUE DONARON */}
      <View style={fourthTabStyles.donaciones}>
        <View style={fourthTabStyles.causasdeonadas}>
          <Text style={fourthTabStyles.numero}>36</Text>
          <Text>Causas Apoyadas</Text>
        </View>
        <View style={fourthTabStyles.paisesdonados}>
          <Image
            source={require("../../assets/img/Vector.png")}
            style={fourthTabStyles.emojiCorazon}
          />
          <Text style={fourthTabStyles.numero}>12</Text>
          <Text>Países apoyados</Text>
        </View>
      </View>

      {/* ULTIMAS DONACIONES */}

      <View style={fourthTabStyles.donacionesUltimas}>
        <Text style={fourthTabStyles.tituloCausas}>
          Ultimas causas apoyadas
        </Text>

        <View style={fourthTabStyles.imagenesComentario}>
          <FlatList
            data={data}
            keyExtractor={({ id }, index) => id}
            renderItem={({ item }) => (
              <UltimasCausas avatar={item.avatar}></UltimasCausas>
            )}
          />
        </View>
      </View>
    </View>
  );
}
const fourthTabStyles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
  },
  tituloPrincipal: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  titulo: {
    fontWeight: "bold",
    fontSize: 35,
  },
  diagrama: {
    flex: 3,
    flexDirection: "column",
    justifyContent: "space-around",
    borderWidth: 2,
    bordercolor: "blue",
  },
  montorecaudado: {
    color: "blue",
    flex: 0.5,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  textoMoneda: {
    color: "blue",
    fontWeight: "bold",
    fontSize: 35,
  },
  descricion: {
    color: "gray",
  },
  donaciones: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  causasdeonadas: {
    flex: 1,
    flexDirection: "row",
  },
  paisesdonados: {
    flex: 1,
    flexDirection: "row",
  },
  numero: {
    fontWeight: "bold",
  },
  donacionesUltimas: {
    width: "100%",
    height: "48.5%",
  },
  tituloCausas: {
    flex: 0.5,
    // textAlign:'center',
    paddingLeft: 50,
    fontWeight: "bold",
    fontSize: 20,
    justifyContent: "center",
  },
  imagenesComentario: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderRadius: 10,
  },
});

const UltimasCausas = ({ avatar }) => {
  return (
    <View style={stylesCausas.containerGrande}>
      <View style={stylesCausas.container}>
        <View style={stylesCausas.cajaGrandeUno}>
          <Text style={stylesCausas.cajaMediana}>holaaa</Text>
        </View>

        <View style={stylesCausas.cajaGrande}>
          <View style={stylesCausas.recaudaciones}>
            <View style={stylesCausas.montosRecaudados}>
              <Text style={stylesCausas.textoAzul}>$ 203.23 </Text>
              <Text style={stylesCausas.textogrey}> recuadados de $2000</Text>
            </View>
            <View style={stylesCausas.diasRestantes}>
              <Text style={stylesCausas.textoAzul}>17</Text>
              <Text style={stylesCausas.textogrey}> dias restantes</Text>
            </View>
          </View>
          <View style={stylesCausas.slider}></View>

          <View style={stylesCausas.footerDonaciones}>
            <Image style={stylesCausas.imagen} source={avatar} />

            <View style={fourthTabStyles.textoNumero}>
              <Text>desde $3456.08</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

const stylesCausas = StyleSheet.create({
  containerGrande: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },

  container: {
    width: "80%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    borderWidth: 2,
    borderColor: "blue",
    margin: 2,
  },
  cajaGrandeUno: {
    width: "20%",
    height: "100%",
  },
  cajaMediana: {
    width: "100%",
    height: "100%",
    backgroundColor: "blue",
  },

  cajaGrande: {
    width: "80%",
    height: "100%",
    flexDirection: "column",
    backgroundColor: "#E5E8E8",
    padding: 5,
  },

  recaudaciones: {
    height: 17,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  montosRecaudados: {
    width: 150,
    height: 12,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
  },

  diasRestantes: {
    width: 100,
    height: 12,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
  },

  textoAzul: {
    color: "blue",
    fontSize: 12,
  },
  textogrey: {
    color: "grey",
    fontSize: 10,
  },

  slider: {
    backgroundColor: "blue",
    height: 5,
    width: "70%",
    marginVertical: 10,
    borderRadius: 5,
    marginRight: "30%",
  },
  footerDonaciones: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  imagen: {
    width: 20,
    height: 20,
    borderRadius: 50,
  },
  textoNumero: {
    width: "20%",
    height: "10%",
  },
});

const renderScene = SceneMap({
  left: FirstRoute,
  middle: SecondRoute,
  right: TerceroRoute,
});

// COMPONENT END

// COMPONENT STYLES
const componentStyles = StyleSheet.create({
  avatar: {
    width: vw(33),
    height: vw(33),
  },
  scene: {
    flex: 1,
  },
});

// COMPONENT STYLES END

// MAIN COMPONENT
const ProfileVerified = () => {
  const [index, setIndex] = React.useState(0);
  const navigation = useNavigation();

  const [routes] = React.useState([
    { key: "left", title: "Posts" },
    { key: "middle", title: "Donations" },
    { key: "right", title: "Stories" },
  ]);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={mainStyles.container}>
        <View style={mainStyles.header}>
          <View style={mainStyles.profileDetails}>
            <View style={mainStyles.backButton}>
              <AntDesign
                name="left"
                size={30}
                color="black"
                fontWeight="bold"
              />
            </View>
            <View style={mainStyles.profileAvatar}>
              <TouchableOpacity onPress={() => {}}>
                <View style={mainStyles.profileAvatarStory}>
                  <View style={mainStyles.profileAvatarContainer}>
                    <Image
                      source={require("../../assets/img/SignLog/logo.png")}
                      style={mainStyles.avatar}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={mainStyles.profileInfo}>
              <View style={mainStyles.nombreyLogo}>
                <Text style={mainStyles.profileInfoUsername}>crickster</Text>

                <Image
                  source={require("../../assets/img/Verificado.svg")}
                  style={mainStyles.imagenVerificacion}
                />
              </View>

              <Text style={mainStyles.profileInfoOccupation}>
                Multimedia Ministry Leader
              </Text>
              <Text style={mainStyles.nombreIglesia}> Iglesia Génesis</Text>
              <View style={mainStyles.grupoPerteneciente}>
                <Text style={mainStyles.cargo}>Alabanza</Text>
                <Text style={mainStyles.cargo}>Logística</Text>
              </View>
              <View style={mainStyles.profileInfoButtons}>
                <TouchableOpacity
                  style={mainStyles.profileInfoButton}
                  onPress={() => {
                    alert("Follow");
                  }}
                >
                  <Text style={mainStyles.profileInfoButtonText}>
                    Editar Perfil
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  style={mainStyles.imagenesReaccion}
                  onPress={() => {
                    alert("Message");
                  }}
                >
                  {/* profileInfoButtonText */}
                  <Image
                    source={require("../../assets/img/upload.png")}
                    style={mainStyles.imagenesBotones}
                  />
                </TouchableOpacity>

                <TouchableOpacity
                  style={mainStyles.imagenesReaccion}
                  onPress={() => {
                    alert("Message");
                  }}
                >
                  <Image
                    source={require("../../assets/img/Vector.png")}
                    style={mainStyles.imagenesBotones}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={mainStyles.profileStats}>
            <View style={mainStyles.profileStatsItem}>
              <Text style={mainStyles.profileStatsNumber}>9,243</Text>
              <Text style={mainStyles.profileStatsText}>Followers</Text>
            </View>
            <View style={mainStyles.profileStatsItem}>
              <Text style={mainStyles.profileStatsNumber}>128</Text>
              <Text style={mainStyles.profileStatsText}>Posts</Text>
            </View>
            <View style={mainStyles.profileStatsItem}>
              <Text style={mainStyles.profileStatsNumber}>69</Text>
              <Text style={mainStyles.profileStatsText}>Causes</Text>
            </View>
          </View>
        </View>

        <View style={mainStyles.body}>
          <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
            style={mainStyles.navivationPaneContainer}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

// MAIN COMPONENT END

// MAIN STYLES
const mainStyles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    backgroundColor: "#fff",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  header: {
    width: "100%",
    height: "40%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  profileDetails: {
    width: "100%",
    height: "30%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  backButton: {
    width: "10%",
    height: "100%",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  profileAvatar: {
    width: "30%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  profileAvatarStory: {
    width: 100,
    height: 100,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    backgroundColor: "#AA4FFB",
    // backgroundColor: 'linear-gradient(144.07deg, #AA4FFB 5.28%, #2A3BF2 51.76%, #44ADFA 109.05%)',
  },
  profileAvatarContainer: {
    width: 95,
    height: 95,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    backgroundColor: "#fff",
  },
  avatar: {
    width: 80,
    height: 80,
    resizeMode: "contain",
    borderRadius: 50,
  },
  profileInfo: {
    width: "60%",
    height: "100%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
    paddingHorizontal: "2%",
  },
  nombreyLogo: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-end",
  },
  profileInfoUsername: {
    fontSize: vw(8),
    fontWeight: "bold",
    color: "#000",
  },
  imagenVerificacion: {
    width: 30,
    height: 30,
  },
  profileInfoOccupation: {
    fontSize: vw(3.5),
    color: "#000",
  },
  nombreIglesia: {
    fontSize: vw(3.5),
    color: "blue",
  },
  grupoPerteneciente: {
    flexDirection: "row",
    justifyContent: "flex-start",
  },
  cargo: {
    fontSize: vw(3.5),
    color: "#000",
    marginHorizontal: 5,
  },
  profileInfoButtons: {
    width: "100%",
    height: "30%",
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  profileInfoButton: {
    width: "40%",
    height: "80%",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    backgroundColor: "blue",
  },
  profileInfoButtonText: {
    fontSize: vw(3.5),
    color: "#fff",
    fontWeight: "bold",
  },

  imagenesReaccion: {
    width: 20,
    height: 20,
  },
  imagenesBotones: {
    width: "100%",
    height: "100%",
  },

  profileStats: {
    width: "100%",
    height: "30%",
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    paddingHorizontal: "5%",
  },
  profileStatsItem: {
    width: "30%",
    height: "100%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  profileStatsNumber: {
    fontSize: vw(8),
    fontWeight: "bold",
    color: "#000",
  },
  profileStatsText: {
    fontSize: vw(3.5),
    color: "#000",
  },

  body: {
    width: "100%",
    height: "60%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  navivationPaneContainer: {
    // marginTop: StatusBar.currentHeight,
    width: "100%",
    height: "100%",
  },
});
// MAIN STYLES END

export default ProfileVerified;
