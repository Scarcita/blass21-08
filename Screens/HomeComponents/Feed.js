import React from "react";
import { StyleSheet, View, Alert } from "react-native";

import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";

import Header from "./FeedComponents/Header";
import Body from "./FeedComponents/Body";
import Navbar from "./FeedComponents/Navbar";
import * as Progress from "react-native-progress";

import * as mimeLib from "react-native-mime-types";
import "react-native-get-random-values";
import { v4 as uuidv4 } from "uuid";
import { useEffect } from "react";

const Feed = ({ navigation, route }) => {
  const [hasUpload, setHasUpload] = React.useState(false);
  // const [uploadFeed, setUploadFeed] = React.useState(false);

  let uploadImage = async ({ cameraShoot, description }) => {
    if (cameraShoot != null) {
      const mime = mimeLib.lookup(cameraShoot);
      const fileName = Date.now() + uuidv4() + "." + mime.split("/")[1];
      var typePost;
      mime == "image/jpeg" ? (typePost = "image") : (typePost = "video");

      // console.log("NAME: " + fileName);
      // console.log("MIME: " + mime);
      // console.log("URI: " + cameraShoot);

      let data = new FormData();
      data.append("media_url", {
        name: fileName,
        type: mime,
        uri: cameraShoot,
      });
      data.append("text", description);
      data.append("user_id", 1);
      data.append("type", typePost);

      let res = await fetch("https://slogan.com.bo/blssd/posts/addMobile", {
        method: "post",
        body: data,
        headers: {
          "Content-Type": "multipart/form-data; ",
        },
      });

      let responseJson = await res.json();
      console.log(responseJson);
      if (responseJson.response.status == 1) {
        Alert.alert("Upload Successful");
        setHasUpload(false);
      }
    } else {
      //if no file selected the show alert
      console.log("Please Select File first");
    }
  };

  useEffect(() => {
    if (route.params?.cameraShoot) {
      // Post updated, do something with `route.params.post`
      // For example, send the post to the server

      console.log(route.params?.cameraShoot);
      setHasUpload(true);
      uploadImage({
        cameraShoot: route.params?.cameraShoot,
        description: route.params?.description,
      });
    }
  }, [route.params?.cameraShoot]);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <Header></Header>
        {hasUpload ? (
          <View style={styles.loading}>
            <Progress.Bar style={{ width: "100%" }} indeterminate={true} />
          </View>
        ) : (
          <></>
        )}
        <Body update={hasUpload}></Body>
        {/* <Navbar></Navbar> */}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loading: {
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default Feed;
