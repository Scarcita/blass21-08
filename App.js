import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";

import * as SecureStore from "expo-secure-store";

import { Provider as AuthProvider } from "./context/AuthContext.js";
import { Context as AuthContext } from "./context/AuthContext";

import Splash from "./Screens/Splash";

import Welcome from "./Screens/Sign-LogScreens/Welcome.js";
import Login from "./Screens/Sign-LogScreens/Login.js";
import Register from "./Screens/Sign-LogScreens/Register.js";
import PersonalData from "./Screens/Sign-LogScreens/PersonalData.js";

import CameraScreen from "./Screens/HomeComponents/FeedComponents/Camera.js";
import Profile from "./Screens/HomeComponents/Profile.js";

const Stack = createStackNavigator();
const SignLogFlow = createStackNavigator();
const Tab = createBottomTabNavigator();

// SVG
import LogoHome from "./assets/svg/Home/home.svg";
import LogoSearch from "./assets/svg/Home/lupa.svg";
import LogoUpload from "./assets/svg/Home/camara.svg";
import LogoComment from "./assets/svg/Home/comentarios.svg";
import LogoProfile from "./assets/svg/Home/perfil.svg";
import Home from "./Screens/Home.js";

function SignLog() {
  return (
    <SignLogFlow.Navigator
      initialRouteName="Welcome"
      screenOptions={{
        headerShown: false,
      }}
    >
      <SignLogFlow.Screen name="Welcome" component={Welcome} />
      <SignLogFlow.Screen name="Login" component={Login} />
      <SignLogFlow.Screen name="Register" component={Register} />
      <SignLogFlow.Screen name="PersonalData" component={PersonalData} />
      <SignLogFlow.Screen name="Home" component={HomeFlow} />
    </SignLogFlow.Navigator>
  );
}

function HomeFlow() {
  return (
    <Tab.Navigator
      screenOptions={() => ({
        headerShown: false,
        activeTintColor: "purple",
        inactiveTintColor: "gray",
      })}
    >
      <Tab.Screen
        name="HomeScreen"
        component={Home}
        tabBarIcon={({ focused, size, color }) => {
          return focused ? (
            <LogoHome width={size} height={size} fill={color} />
          ) : (
            <LogoComment width={size} height={size} fill={color} />
          );
        }}
        screenOptions={{
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="Search"
        component={Splash}
        tabBarIcon={({ focused, size }) => {
          iconName = focused ? "search" : "search";
          return <LogoSearch size={size} />;
        }}
      />
      <Tab.Screen
        name="Upload"
        component={CameraScreen}
        tabBarIcon={({ focused, size }) => {
          iconName = focused ? "upload" : "upload";
          return <LogoUpload size={size} />;
        }}
      />
      <Tab.Screen
        name="Chat"
        component={Splash}
        tabBarIcon={({ focused, size }) => {
          iconName = focused ? "chat" : "chat";
          return <LogoComment size={size} />;
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        tabBarIcon={({ focused, size }) => {
          iconName = focused ? "profile" : "profile";
          return <LogoProfile size={size} />;
        }}
      />
    </Tab.Navigator>
  );
}

function App() {
  const { state, restoreToken } = React.useContext(AuthContext);
  console.log(state);

  React.useEffect(() => {
    const bootstrapAsync = async () => {
      let userToken;
      try {
        userToken = await SecureStore.getItemAsync("userToken");
      } catch (e) {}
      restoreToken({ userToken });
    };
    bootstrapAsync();
  }, []);
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        {state.isLoading ? (
          // We haven't finished checking for the token yet
          <Stack.Group>
            <Stack.Screen name="Splash" component={Splash} />
          </Stack.Group>
        ) : state.userToken == null ? (
          // No token found, user isn't signed in
          // <Stack.Screen name="Home" component={HomeFlow} />
          <Stack.Screen name="SignLog" component={SignLog} />
        ) : (
          // User is signed in
          <Stack.Group>
            <Stack.Screen name="Home" component={HomeFlow} />
          </Stack.Group>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default () => {
  return (
    <SafeAreaProvider>
      <SafeAreaView style={{ flex: 1 }}>
        <AuthProvider>
          <App />
        </AuthProvider>
      </SafeAreaView>
    </SafeAreaProvider>
  );
};

